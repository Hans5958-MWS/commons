import re

import importlib.util
import sys
spec = importlib.util.spec_from_file_location("title_case", "pwb/user_modules/title_case.py")
title_case = importlib.util.module_from_spec(spec)
sys.modules["title_case"] = title_case
spec.loader.exec_module(title_case)

def tidy_subject(str):

	str = re.sub(' {2,}', ' ', str)
	str = re.sub('^\s+', '', str)
	str = re.sub('\s+$', '', str)
	str = re.sub('\s+', ' ', str)
	str = re.sub('\s*[/]\s*', '/', str)
	str = re.sub('\.+$', '', str)
	str = title_case.title(str)
	str = re.sub('(tentang|No\.|Thn.)\s*', '\g<1> ', str)
	str = re.sub('^tentang', '', str)
	str = re.sub('[.]$', '', str)
	str = str.strip()
	# print(subject)
	# print(index_dpr[key]['subject'])

	return str