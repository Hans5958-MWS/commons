import json
import re
from _modules import tidy_subject

index: dict = {}

def keys_sorted_funct(key: str):
	try:
		year, number = key.split('-')
		number = number.rjust(3, '0')
		# print(year, number)
		return f'{year}{number}'
	except Exception:
		return key

for year in range(1945, 2023):
	with open(f'data/uu/index/{year}.json', 'r', encoding='utf-8') as f:
		index_year: dict = json.loads(f.read())

	for key in index_year:
		current_index = index_year[key].copy()

		current_index['number'] = current_index.pop('nomor')
		current_index['year'] = current_index.pop('tahun')
		current_index['url'] = current_index.pop('pdfUrl')
		current_index['subject'] = current_index.pop('tentang').strip()
		current_index['author'] = '{{Creator:Government of Indonesia}}'
		current_index.pop('judul')
		current_index.pop('deskripsi')
		current_index.pop('indexUrl')

		if 'indexData' in current_index:
			if 'tanggalUndang' in current_index['indexData'] and current_index['indexData']['tanggalUndang']:
				current_index['date'] = current_index['indexData']['tanggalUndang']
			elif 'tanggalSah' in current_index['indexData'] and current_index['indexData']['tanggalSah']:
				current_index['date'] = current_index['indexData']['tanggalSah']

		current_index.pop('indexData')

		if 'subject' in current_index:
			current_index['subject'] = tidy_subject(current_index['subject'])

		current_index['title'] = f"Undang-Undang Republik Indonesia Nomor {current_index['number']} Tahun {current_index['year']}"
		current_index['description'] = f"{current_index['title']} tentang {current_index['subject']}"

		current_index['title_en'] = f"Law of the Republic of Indonesia Number {current_index['number']} of {current_index['year']}"
		current_index['description_en'] = current_index['title_en']

		current_index['categories'] = [
			f"Law of the Republic of Indonesia of {current_index['year']}|{str(current_index['number']).rjust(2, '0')}",
		]

		if 'date' in current_index and current_index['date']:
			current_index['categories'].append(current_index['date'])

		if 'additionalUrls' in current_index:
			current_index.pop('additionalUrls')

		index[key] = current_index

with open('data/uu/index-ready.json', 'w', encoding='utf8') as f:
	f.write(json.dumps(index))