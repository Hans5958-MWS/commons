import json
import re
from _modules import tidy_subject

law_code = 'pp'
bphn_file_path = f'data/{law_code}/index-bphn.json'
dpr_file_path = f'data/{law_code}/index-dpr.json'
result_file_path = f'data/{law_code}/index-cleaner-py.json'

def generate_pdf_url(number, year):
	return f'https://bphn.jdihn.go.id/common/dokumen/{year}pp{str(number).rjust(3, "0")}.pdf'

with open(bphn_file_path, 'r', encoding='utf8') as f:
	index_bphn: dict = json.loads(f.read())
with open(dpr_file_path, 'r', encoding='utf8') as f:
	index_dpr: dict = json.loads(f.read())

index = {}

def keys_sorted_funct(key: str):
	try:
		year, number = key.split('-')
		number = number.rjust(3, '0')
		# print(year, number)
		return f'{year}{number}'
	except Exception:
		return key

current_number = 1
current_year = 1945

for key in index_bphn.keys():
	current_index = index_bphn[key].copy()

	if 'error' in current_index:
		# index[key] = current_index
		continue

	if not current_index['date'] or not current_index['date'].strip():
		if key in index_dpr and 'date' in index_dpr[key] and index_dpr[key]['date'].strip():
			current_index['date'] = index_dpr[key]['date']
			print(f"No date on {key}, got it from DPR.")
		else:
			current_index['date'] = None
			print(f"No date on {key}.")
	# print(current_index['date'])

	if 'subject' not in current_index and key in index_dpr:
		current_index['subject'] = index_dpr[key]['subject']
		print(f"No subject on {key}, got it from DPR")

	# if key in index_dpr:
	# 	current_index['subject'] = title_case.title(index_dpr[key]['subject'])
	# 	# print(f"No subject on {key}, got it from DPR")

	index[key] = current_index

	# print(index[key]['date'])

for key in index_dpr.keys():
	if key in index:
		continue

	current_index = index_dpr[key].copy()

	current_index['pdfUrl'] = generate_pdf_url(current_index['number'], current_index['year'])

	index[key] = current_index

keys_sorted = sorted(index.keys(), key=keys_sorted_funct)
index_sorted = {}

for key in keys_sorted:
	current_index = index[key].copy()

	if (current_index['number'] is None):
		continue
	if (current_index['number'] != current_number + 1 and current_index['year'] == current_year):
		print(f"WARNING: {current_index['year']}-{current_index['number'] + 1} does not exists!")

	current_number = current_index['number']
	current_year = current_index['year']

	if 'subject' in current_index:
		current_index['subject'] = tidy_subject(current_index['subject'])

	if 'additionalUrls' in current_index:
		current_index.pop('additionalUrls')

	index_sorted[key] = current_index

index = index_sorted

with open(result_file_path, 'w', encoding='utf8') as f:
	f.write(json.dumps(index))