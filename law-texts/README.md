# law-texts

This script uploads the law texts of Indonesia to Wikimedia Commons, with appropriate information.

## Overview

### Uploading texts

- Run `get-data.js` to get data by scraping the associated website  
  `node js/get-data.js`

- Run `build-missing-files.js` to have a list if existing files by comparing to Wikisource  
  `node js/build-missing-files.js`

- Add additional files that already existed by checking Commons

- Run `upload_texts` using Pywikibot to start uploading.  
  `py pwb/pwb.py upload_texts -index:data/uu/index-ready.json -skipkeys:data/uu/existing_codes.txt -always -simulate`

### Updating information of existing files

TBA

- Run `info_updater` using Pywikibot to start updating.
  e.g. `py pwb\pwb.py info_updater -index:data/uu/index-ready.json -category:"Law of the Republic of Indonesia of 2018" -simulate`

### Preparing categories

TBA

```
py pwb/pwb.py pagefromfile -file:cats-uu.txt -notitle -summary:"[[User:Bot5958/TaskRedirect/1|Task 1]]: Prepare Indonesia law categories by year: [[:Category:Law of the Republic of Indonesia|UU]]" -simulate
```

## License

MIT