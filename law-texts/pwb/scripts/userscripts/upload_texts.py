#!/usr/bin/python3
"""
Simplified script of upload.py to upload Indonesia law texts to Wikimedia Commons

The following parameters are supported:

  -indexfolder  Path to folder containing all the directory.
  -skipkeys     Path to file containing file keys to be skipped on upload
  -keep         Keep the filename as is.
  -abortonwarn  Abort upload on the specified warning type. If no warning type
                is specified, aborts on any warning.
  -ignorewarn   Ignores specified upload warnings. If no warning type is
                specified, ignores all warnings. Use with caution
  -async        Make potentially large file operations asynchronous on the
                server side when possible.
  -always       Don't ask the user anything. This will imply -keep and
                -noverify and require that either -abortonwarn or -ignorewarn
                is defined for all. It will also require a valid file name and
                description. It'll only overwrite files if -ignorewarn includes
                the 'exists' warning.
  -summary      Pick a custom edit summary for the bot.

It is possible to combine -abortonwarn and -ignorewarn so that if the specific
warning is given it won't apply the general one but more specific one. So if it
should ignore specific warnings and abort on the rest it's possible by defining
no warning for -abortonwarn and the specific warnings for -ignorewarn. The
order does not matter. If both are unspecific or a warning is specified by
both, it'll prefer aborting.

If any other arguments are given, the first is either URL, filename or
directory to upload, and the rest is a proposed description to go with the
upload. If none of these are given, the user is asked for the directory, file
or URL to upload. The bot will then upload the image to the wiki.

The script will ask for the location of an image(s), if not given as a
parameter, and for a description.
"""
#
# (C) Pywikibot team, 2003-2021
#
# Distributed under the terms of the MIT license.
#
import json
import time
import requests

import pywikibot
from pywikibot.bot import suggest_help
from pywikibot.specialbots import UploadRobot

HEADERS = {
	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:104.0) Gecko/20100101 Firefox/104.0',
}

def main(*args: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	url: str = ''
	description: list = []
	summary: str = "[[User:Bot5958/TaskRedirect/1|Task 1]]: Add Indonesian law text: [[:Category:Law of the Republic of Indonesia|UU]]"
	filename: str = False
	always: bool = False
	keep: bool = False
	aborts: set = set()
	ignorewarn: set = set()
	asynchronous: bool = False
	start_year = 0
	start_number = 0

	index_file = '' 
	skip_keys = []

	# process all global bot args
	# returns a list of non-global args, i.e. args for upload.py
	local_args = pywikibot.handle_args(args)
	for option in local_args:
		arg, _, value = option.partition(':')
		if arg == '-always':
			always = True
		if arg == '-keep':
			keep = True
		elif arg == '-filename':
			filename = value
		elif arg == '-summary':
			summary = value
		elif arg == '-abortonwarn':
			if value and aborts is not True:
				aborts.add(value)
			else:
				aborts = True
		elif arg == '-ignorewarn':
			if value and ignorewarn is not True:
				ignorewarn.add(value)
			else:
				ignorewarn = True
		elif arg == '-async':
			asynchronous = True
		elif arg == '-start':
			start_vals = value.split('-')
			start_year = int(start_vals[0])
			start_number = int(start_vals[1])
		# elif arg == '-indexfolder':
		# 	if not value:
		# 		pywikibot.error('No index folder given.')
		# 	else:
		# 		index_files = [join(value, f) for f in listdir(value) if isfile(join(value, f)) and f.endswith('.json')]
		elif arg == '-index':
			if not value:
				pywikibot.error('No index given.')
			else:
				index_file = value
		elif arg == '-skipkeys':
			with open(value, 'r', encoding='utf-8') as f:
				skip_keys = f.read().split()

	count = 0
	time_to_upload = 0

	law_index = None
	with open(index_file, 'r', encoding='utf-8') as texts_data_text:
		law_index = json.loads(texts_data_text.read())

	for key in law_index:
		if key in skip_keys:
			continue
		key_vals = key.split('-')
		key_year = int(key_vals[0])
		key_number = int(key_vals[1])
		if key_year < start_year or (key_year == start_year and key_number < start_number):
			continue

		law_data = law_index[key]

		filename = law_data['title'] + '.pdf'
		pywikibot.info('Now uploading: ' + key)
		pywikibot.info('File Name: ' + filename)
		description = [
			"=={{int:filedesc}}==",
			"{{Information",
			"| Description    =",
			"{{id|" + law_data['description'] + "}}",
			"{{en|" + law_data['description_en'] + "}}",
			f"| Source         = {law_data['url']}",
		]
		if 'date' in law_data:
			description.append(f"| Date           = {law_data['date']}")

		for line in [
			f"| Author         = {law_data['author']}",
			"}}",
			"",
			"=={{int:license-header}}==",
			"{{PD-IDNoCopyright}}",
		]:
			description.append(line)

		if 'categories' in law_data:
			description.append("")

			for category in law_data['categories']:
				description.append(f"[[Category:{category}]]")

		description = '\n'.join(description)

		# pywikibot.log(filename)
		# pywikibot.log(url)
		# pywikibot.log("==== Description ====")
		# pywikibot.log(description)
		# pywikibot.log('================================')

		# if count > 1 - 1:
		# 	break
		# count += 1

		try:

			pywikibot.info('Downloading from source...')
			r = requests.get(law_data['url'], headers=HEADERS)
			with open("temp_upload.pdf", 'wb') as f:
				for chunk in r.iter_content(1024):
					f.write(chunk)
		
		except Exception:

			pywikibot.warning('Failed to download file! Skipping...')
			continue

		url = ['temp_upload.pdf']

		if time.time() < time_to_upload:
			delay = time_to_upload - time.time()
			print(f"Waiting for {delay} seconds...")
			time.sleep(time_to_upload - time.time())

		pywikibot.info(f'Uploading...')

		try:
			# time_to_upload = time.time() + 5
			bot = UploadRobot(url, description=description, use_filename=filename,
				aborts=aborts,
				ignore_warning=ignorewarn,
				asynchronous=asynchronous,
				always=always, summary=summary, keep_filename=keep, verify_description=keep)
			bot.run()
			pywikibot.info(f'Upload done!')
		except Exception:
			pywikibot.warning('Error on uploading!')


if __name__ == '__main__':
	main()
