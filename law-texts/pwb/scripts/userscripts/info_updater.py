#!/usr/bin/python3
"""
Updates information of Indonesia law texts.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-indexfolder  Path to folder containing all the directory.
-always       The bot won't ask for confirmation when putting a page
-summary      Set the action summary message for the edit.
-major        If used, the edit will be saved without the "minor edit" flag

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
import json
import re
from os import listdir
from os.path import isfile, join
from mwparserfromhell.wikicode import Wikicode, Template, Wikilink
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
import mwparserfromhell
from user_modules import title_case

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': '[[User:Bot5958/TaskRedirect/1|Task 1]]: Update Indonesia law text information: [[:Category:Law of the Republic of Indonesia|UU]]',
	'minor': True,
	'index': ''
}

CODE_REGEX = r'(\d{1,2})[^\d]+([1-2]\d{3})[^\d](?:[^\d]|$)'
CODE_OPPOSITE_REGEX = r'([1-2]\d{3})[^\d]+(\d{1,2})(?:[^\d]|$)'
TENTANG_REGEX = r'(?:tentang|TENTANG|Tentang) (.+)\b'
TENTANG_EN_REGEX = r'\{\{\s*en.+ (?:on|about) (.+)\b'
DATE_CAT_REGEX = r'\[\[\s*Category:(\d{4}-\d{1,2}-\d{1,2})\s*\]\]'

def get_param_with_aliases(template: Template, aliases: list):
	for alias in aliases:
		if template.has(alias):
			return template.get(alias)
	return None

class LawTextInfoUpdater(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
):

	"""
	Update law text infoormation.
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		title = page.title()
		text = page.text
		code: Wikicode = mwparserfromhell.parse(text)

		# page.protection()
		# if not page.has_permission():
		# 	pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
		# 	return True

		code_match = re.search(CODE_REGEX, title)
		code_opposite_match = re.search(CODE_OPPOSITE_REGEX, title)
		code_info_match = None
		code_info_opposite_match = None
		info_template = code.filter_templates(
			matches=lambda x: x.name.matches("Information")
		)
		if info_template:
			info_template: Template = info_template[0]
			desc_param = get_param_with_aliases(info_template, ['description', 'Description'])
			if desc_param:
				code_info_match = re.search(CODE_REGEX, str(desc_param.value))
				code_info_opposite_match = re.search(CODE_OPPOSITE_REGEX, str(desc_param.value))

		if not code_match and not code_opposite_match and not code_info_match and not code_info_opposite_match:
			print("Number and year can't be inferred! Skipping...")

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		
		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)
		code: Wikicode = mwparserfromhell.parse(text)

		modified_keys = []

		# Get number and year

		code_match = re.search(CODE_REGEX, title)
		code_opposite_match = re.search(CODE_OPPOSITE_REGEX, title)
		code_info_match = None
		code_info_opposite_match = None
		info_template = code.filter_templates(
			matches=lambda x: x.name.matches("Information")
		)
		if info_template:
			info_template: Template = info_template[0]
			desc_param = get_param_with_aliases(info_template, ['description', 'Description'])
			if desc_param:
				code_info_match = re.search(CODE_REGEX, str(desc_param.value))
				code_info_opposite_match = re.search(CODE_OPPOSITE_REGEX, str(desc_param.value))

		if code_match:
			nomor: int = int(code_match.group(1))
			tahun: int = int(code_match.group(2))
		elif code_opposite_match:
			nomor: int = int(code_opposite_match.group(2))
			tahun: int = int(code_opposite_match.group(1))
		elif code_info_match:
			nomor: int = int(code_info_match.group(1))
			tahun: int = int(code_info_match.group(2))
		elif code_info_opposite_match:
			nomor: int = int(code_info_opposite_match.group(2))
			tahun: int = int(code_info_opposite_match.group(1))

		with open(self.opt.index, 'r', encoding='utf-8') as f:
			index = json.loads(f.read())

		law_data = index[f'{tahun}-{nomor}']

		# # Remove dot on end of some fields
		# law_data['subject'] = re.sub(f"\.+$", '', law_data['subject'])
		# law_data['deskripsi'] = re.sub(f"\.+$", '', law_data['deskripsi'])

		info_template = code.filter_templates(
			matches=lambda x: x.name.matches("Information")
		)

		if info_template:
			info_template: Template = info_template[0]

			# Update description, if all caps, prefer using available language if possible
			desc_param = get_param_with_aliases(info_template, ['description', 'Description'])
			desc_value = "\n{{id|" + str(law_data['title']) + " tentang " + law_data['subject'] + "}}"
			if desc_param:

				desc_value_old = str(desc_param.value)
				
				# Avoid all caps title by trying to use the existing description, or capitalize it
				subject_match = re.search(TENTANG_REGEX, str(desc_param.value))
				subject_alt = law_data['subject']
				if subject_match:
					subject_alt = subject_match.group(1)
				subject_alt = re.sub(f"\.+$", '', subject_alt)
				caps_percentage = (sum(1 for char in law_data['subject'] if char.isupper()))/len(law_data['subject'])
				if caps_percentage > 0.80:
					desc_value = "\n{{id|" + str(law_data['title']) + " tentang " + subject_alt + "}}"
				
				subject_en_match = re.search(TENTANG_EN_REGEX, str(desc_param.value))
				if subject_en_match:
					desc_value += "\n{{en|" + law_data['title_en'] + " on " + subject_en_match.group(1) + "}}\n"
				else:
					desc_value += "\n{{en|" + law_data['title_en'] + "}}\n"
				desc_param.value = desc_value

				if desc_value != desc_value_old:
					modified_keys.append('description')

			else:
				info_template.add('description', desc_value)
				modified_keys.append('description')

			# Update date
			date_param = get_param_with_aliases(info_template, ['date', 'Date'])
			# print(law_data)
			publish_date = law_data['date']
			date_value = publish_date + '\n'
			if date_param:

				date_value_old = str(date_param.value)

				if date_value_old.startswith(' '):
					date_value = ' ' + date_value
				date_param.value = date_value

				if date_value != date_value_old:
					modified_keys.append('date')
			else:
				info_template.add('date', date_value)
				modified_keys.append('date')

			# Update author
			author_param = get_param_with_aliases(info_template, ['author', 'Author'])
			author_value: str = law_data['author'] + "\n"
			if author_param:

				author_value_old = str(author_param.value)
				if author_value_old.startswith(' ') or str(get_param_with_aliases(info_template, ['date', 'Date']).value).startswith(' '):
					author_value = ' ' + author_value
				author_param.value = author_value

				if author_value != author_value_old:
					modified_keys.append('author')

			else:
				info_template.add('author', author_value)
				modified_keys.append('author')

			# Separate licensing
			permission_param = get_param_with_aliases(info_template, ['permission', 'Permission'])
			if permission_param and "int:license-header" not in text:
				code.insert_after(info_template, "\n=={{int:license-header}}==\n" + str(permission_param.value).strip())
				info_template.remove(permission_param.name)
				modified_keys.append('license')
			 
		# Use {{PD-IDNoCopyright}} instead of {{PD-IDGov}}
		copyright_template = code.filter_templates(
			matches=lambda x: x.name.matches("PD-IDGov") or x.name.matches("PD-IDgov") or x.name.matches("DU-PemerintahIndonesia")
		)

		if copyright_template:
			copyright_template: Template = copyright_template[0]
			copyright_template.name = "PD-IDNoCopyright"
			modified_keys.append('license')

		# Update categories

		category_nodes = code.ifilter(True, matches=lambda node: type(node) is Wikilink and 'Category:' in str(node.title))
		for node in category_nodes:
			cat_name = str(node.title).strip().replace('Category:', '')
			# index = code.index(node)
			if "Law of the Republic of Indonesia of Indonesia" == cat_name or \
				"Law of the Republic of Indonesia" == cat_name or \
				"Law of Indonesia"  == cat_name or \
				cat_name.startswith('Indonesia in') or \
				"Law" == cat_name or \
				"PDF" in cat_name:
				code.remove(node)
				modified_keys.append('category')

		# Ensure "Law of... of [year]" are correct
		category_nodes = list(code.ifilter(True, matches=lambda node: type(node) is Wikilink and 'Category:' in str(node.title)))
		year_category = [node for node in category_nodes if str(node.title).strip() == f"Category:Law of the Republic of Indonesia of {tahun}"]
		if year_category:
			old_value = str(year_category[0].text)
			new_value = str(nomor).rjust(2, '0')
			year_category[0].text = new_value
			if old_value != new_value:
				modified_keys.append('category')
		else:
			code.append(f'\n[[Category:Law of the Republic of Indonesia of {tahun}|{str(nomor).rjust(2, "0")}]]')
			modified_keys.append('category')

		# Add date category on publishing
		if not [node for node in category_nodes if node.title == f"Category:{publish_date}"]:
			code.append(f'\n[[Category:{publish_date}]]')
			modified_keys.append('category')
		
		# Remove extra newlines due to category removal
		text = re.sub(r'\n{3,}', '\n\n', str(code))
		# print(text)
		# print(text[:1000])

		summary_new = self.opt.summary + ": " + ', '.join(list(set(modified_keys)))

		self.put_current(text, summary=summary_new, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary', 'index'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = LawTextInfoUpdater(generator=gen, **args)
		bot.run()

if __name__ == '__main__':
	main()
