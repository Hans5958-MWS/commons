import axios from "axios";
import * as cheerio from "cheerio";
import fs from "fs-extra";

const jenisPeraturanQuery = "PENETAPAN+PRESIDEN"
const indexFilePath = `data/penpres/index_bphn.json`
const searchResultType = 'PENPRES'
const defaultPdfLink = 'https://bphn.jdihn.go.id/common/dokumen/%year%pnpr%num3%.pdf'

const titleRegex = /^Penetapan\s*Presiden\s*Nomor\s*(\d+)\s*Tahun\s*(\d+)\s*tentang\s*(.+)\.?\s*$/i
const numberRegex = /^[^\d]+(\d+)[^\d]+(\d+)/
const subjectRegex = /te?n?t?a?n?g\s*(.+)\.?\s*/

const months = "Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember".split(' ')

const dateStringToISO = dateString => {
	return dateString.replace(/(\d+) (\w+) (\d+)/, (str, d, m, y) => `${y}-${(months.indexOf(m) + 1).toString().padStart(2, '0')}-${d.padStart(2, '0')}`)
}

const getIndex = async indexUrl => {
	const response = await axios.get(indexUrl)
	const $ = cheerio.load(response.data)
	const returnData = {
		date: null
	}

	const tanggalPenetapan = $('div.col-lg-6:nth-child(2) > span:nth-child(2)').first().text()
	const tanggalPengundangan = $('div.col-lg-6:nth-child(3) > span:nth-child(2)').first().text()
	const author = $('div.col-lg-6:nth-child(9) > span:nth-child(2)').first().text() || null

	returnData.author = author

	if (tanggalPengundangan) {
		returnData.date = tanggalPengundangan
	} else if (tanggalPenetapan) {
		returnData.date = dateStringToISO(tanggalPenetapan)
	}
	
	for (let el of [...$('div.widget:nth-child(3) > ul:nth-child(2) > a')]) {
		el = $(el)
		if (returnData.pdfUrl === undefined) {
			returnData.pdfUrl = 'https://bphn.jdihn.go.id' + el.attr('href')
			continue
		}
		if (returnData.additionalUrls === undefined) {
			returnData.additionalUrls = {
				abstract: 'https://bphn.jdihn.go.id' + el.attr('href')
			}
			continue
		}
		returnData.additionalUrls[el.text()] = el.attr('href')
		
	}

	return returnData

}

const go = async () => {

	const masterData = {}

	let page = 1

	while (page !== 0) {
		await new Promise(r => setTimeout(r, 500));

		console.log('Doing page ' + page)
		const response = await axios.get(`https://bphn.jdihn.go.id/dokumen/index?DokumenSearch%5Bjudul%5D=&DokumenSearch%5Btipe_dokumen%5D=1&DokumenSearch%5Bjenis_peraturan%5D=${jenisPeraturanQuery}&DokumenSearch%5Bnomor_peraturan%5D=&DokumenSearch%5Btahun_terbit%5D=&DokumenSearch%5Bstatus_terakhir%5D=&per-page=10&page=${page}`)
		const $ = cheerio.load(response.data)
		
		for (const el of [...$('.item')]) {
			await new Promise(r => setTimeout(r, 500));

			let number, year, subject

			const type = $(el).find('a').first().text()
			if (type !== searchResultType) continue

			const link = $(el).find('.text-primary')

			const indexUrl = 'https://bphn.jdihn.go.id' + link.attr('href')
			console.log(indexUrl)

			console.log(link.text())

			try {
				const titleMatch = titleRegex.exec(link.text())
				number = parseInt(titleMatch[1])
				year = parseInt(titleMatch[2])
				subject = titleMatch[3]
			} catch (e) {
				console.warn('Error on parsing title, use lax parsing')
				try {
					const numberMatch = numberRegex.exec(link.text())
					number = parseInt(numberMatch[1])
					year = parseInt(numberMatch[2])
				} catch (e) {
					console.error('Error on getting number, skipping!')
					masterData[link.text()] = {
						indexUrl,
						error: true
					}
					continue
				}
				try {
					const subjectMatch = subjectRegex.exec(link.text())
					subject = subjectMatch[1]
				} catch (e) {
					console.warn(`Subject not found on ${year}-${number}!`)
				}
			}


			masterData[`${year}-${number}`] = {}
			console.log(`${year}-${number} get!`)

			let indexData = {}
			try {
				indexData = await getIndex(indexUrl)
			} catch (e) {
				console.warn(`Failed to get index from ${year}-${number}!`)
				console.info(e)
				indexData = {}
			}
		
			masterData[`${year}-${number}`] = {
				year,
				number,
				subject,
				pdfUrl: defaultPdfLink.replace('%year%', year).replace('%num3%', number.toString().padStart(3, '0')),
				additionalUrls: {
					index: indexUrl
				},
				...indexData
			}

			if (indexData.additionalUrls) {
				masterData[`${year}-${number}`].additionalUrls = {
					index: indexUrl,
					...indexData.additionalUrls
				}
			}

			console.log(masterData[`${year}-${number}`])

		}

		if ($('.next.disabled').length === 0) page += 1
		else page = 0

		fs.outputJSONSync(indexFilePath, masterData)


	}
	
}

await go()