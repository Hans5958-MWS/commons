import axios from "axios"
import fs from "fs-extra"

const codeExistingFile = 'codes-existing.txt'
const existingFile = 'existing.txt'

fs.outputFileSync(codeExistingFile, '')
fs.outputFileSync(existingFile, '')

const go = async tahun => {

	const rulesData = fs.readJsonSync('data/uu/' + tahun + '.json')

	for (const rule of Object.values(rulesData)) {
		const {
			tahun, nomor, judul, tentang, deskripsi, pdfUrl, indexUrl, indexData
		} = rule
		// const exists = await checkIfExists('https://id.wikisource.org/wiki/' + judul.replaceAll(' ', '_'))
		try {
			console.log(`Checking ${tahun}-${nomor}...`)
			const pageResponse = await axios.get(`https://id.wikisource.org/w/api.php?action=query&format=json&prop=revisions&titles=${judul}&formatversion=2&rvprop=content&rvslots=*`)
			const pageWikitext = pageResponse.data.query.pages[0].revisions[0].slots.main.content
			// console.log(pageWikitext)
			const fileName = pageWikitext.match(/index *= *"(.*?)"/)?.[1]
			if (fileName) {
				console.log(`${tahun}-${nomor} exists with file name ${fileName}`)
				fs.appendFileSync(existingFile, `${tahun}-${nomor}: ${fileName}\n`)
			} else {
				console.log(`${tahun}-${nomor} does not exist!`)
				fs.appendFileSync(codeExistingFile, `${tahun}-${nomor}\n`)
			}
		} catch (e) {
			console.log(e)
			console.log(`${tahun}-${nomor} check failed! Assuming missing...`)
			fs.appendFileSync(codeExistingFile, `${tahun}-${nomor}\n`)
		}
		await new Promise(r => setTimeout(r, 500));
	}
}

for (let i = 1945; i < 2023; i++) {
	await go(i)
}

/*

=={{int:filedesc}}==
{{Information
| Description    = $DESCRIPTOR$
| Source         = $URL$
| Author         = {{id|Pemerintah Indonesia}} {{en|Government of Indonesia}}
| Permission     = {{PD-IDNoCopyright}}
}}

[[Category:Law of the Republic of Indonesia]]

*/