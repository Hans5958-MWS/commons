import axios from "axios"
import fetch from "node-fetch"
import fs from "fs-extra"

const tahun = 2019

const checkIfExists = async (url) => {
	axios.get(url)
		.catch(el => console.log(el))
	try {
		await axios.get(url)
		return true
	} catch {
		return false
	}
}

const rulesData = fs.readJsonSync(tahun + '.json')
let tableContent = []

await Promise.all(Object.values(rulesData).map(async rule => {
	tableContent.push('')
	const {
		tahun, nomor, judul, tentang, deskripsi, pdfUrl, indexUrl, indexData
	} = rule
	const exists = await checkIfExists('https://id.wikisource.org/wiki/' + judul.replaceAll(' ', '_'))
	tableContent[nomor - 1] = `|-
|[[${judul}|${nomor} Tahun ${tahun}]]${exists ? '' : `<sup>[[:File:${judul}.pdf|[pdf]]]</sup>`}
|${tentang}
|${indexData.noLN}
|${indexData.noTLN}
|`
	console.log(`${tahun}-${nomor}: ${exists}`)
}))

console.log(tableContent.join('\n'))

/*

https://url2commons.toolforge.org/

=={{int:filedesc}}==
{{Information
| Description    = $DESCRIPTOR$
| Source         = $URL$
| Author         = {{id|Pemerintah Indonesia}} {{en|Government of Indonesia}}
}}

=={{int:license-header}}==
{{PD-IDNoCopyright}}

[[Category:Law of the Republic of Indonesia]]

*/