import axios from "axios";
import * as cheerio from "cheerio";
import fs from "fs-extra";

let masterData = {}

const go = async (tahun, codeSite, codeData) => {

	const response = await axios.get(`https://www.dpr.go.id/jdih/${codeSite}/year/${tahun}`)
	const $ = cheerio.load(response.data)

	for (const el of [...$('tr.list')]) {
		const nomor = Number.parseInt($(el).find('td:nth-of-type(1)').text().trim())
		const tentang = $(el).find('.text').text().trim()
		const date = $(el).find('td:nth-child(3)').text().trim().split('-').sort(_ => -1).join('-')
	
		masterData[`${tahun}-${nomor}`] = {}
		console.log(`${tahun}-${nomor} get!`)
	
		masterData[`${tahun}-${nomor}`] = {
			year: tahun,
			number: nomor,
			date,
			subject: tentang
		}

	}

	await new Promise(r => setTimeout(r, 500));

	fs.outputJSONSync(`data/${codeData}/index-dpr.json`, masterData)

}

masterData = {}
for (let i = 1945; i < 2023; i++) {
	await go(i, 'pp', 'pp')
}
masterData = {}
for (let i = 1945; i < 2023; i++) {
	await go(i, 'perpu', 'perpu')
}
masterData = {}
for (let i = 1945; i < 2023; i++) {
	await go(i, 'perpres', 'perpres')
}
masterData = {}
for (let i = 1945; i < 2023; i++) {
	await go(i, 'keppres', 'keppres')
}
masterData = {}
for (let i = 1945; i < 2023; i++) {
	await go(i, 'inpres', 'inpres')
}