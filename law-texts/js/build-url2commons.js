import axios from "axios"
import fs from "fs-extra"

const checkIfExists = async (url) => {
	try {
		await axios.get(url)
		return true
	} catch {
		return false
	}
}

let url2CommonsText = ''
let codes = ''

fs.outputFileSync('url2Commons.txt', '')
fs.outputFileSync('codes.txt', '')

const go = async tahun => {

	const rulesData = fs.readJsonSync('uu/' + tahun + '.json')

	url2CommonsText = ''
	codes = ''

	for (const rule of Object.values(rulesData)) {
		const {
			tahun, nomor, judul, tentang, deskripsi, pdfUrl, indexUrl, indexData
		} = rule
		const exists = await checkIfExists('https://id.wikisource.org/wiki/' + judul.replaceAll(' ', '_'))
		if (!exists) {
			let fullDescription = ""
			fullDescription += `{{id|${deskripsi}}} {{en|Law of the Republic of Indonesia Number ${nomor} of ${tahun}}} | Date = ${indexData.tanggalUndang}`
			fs.appendFileSync("url2Commons.txt",  `${pdfUrl} ${judul}.pdf|${fullDescription}\n`)
			fs.appendFileSync("codes.txt", `${tahun}-${nomor}\n`)
		}
		console.log(`${tahun}-${nomor}: ${exists}`)
		await new Promise(r => setTimeout(r, 500));
	}
}

for (let i = 1945; i < 2023; i++) {
	await go(i)
}

/*

=={{int:filedesc}}==
{{Information
| Description    = $DESCRIPTOR$
| Source         = $URL$
| Author         = {{id|Pemerintah Indonesia}} {{en|Government of Indonesia}}
| Permission     = {{PD-IDNoCopyright}}
}}

[[Category:Law of the Republic of Indonesia]]

*/