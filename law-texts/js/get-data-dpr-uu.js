import axios from "axios";
import * as cheerio from "cheerio";
import fs from "fs-extra";
import chrono from 'chrono-node'

const months = "Januari Februari Maret April Mei Juni Juli Agustus September Oktober Nopember Desember".split(' ')

const dateStringToISO = dateString => {
	return dateString.replace(/(\d+) (\w+) (\d+)/, (str, d, m, y) => `${y}-${(months.indexOf(m) + 1).toString().padStart(2, '0')}-${d.padStart(2, '0')}`)
}

const getIndex = async indexUrl => {
	const response = await axios.get(indexUrl)
	const $ = cheerio.load(response.data)
	const returnData = {}

	$('.keterangan').children().each((index, el) => {
		const key = $(el).find('.ket-title').text().trim()
		const value = $(el).find('.input').text().replace(': ', '').trim()
		if (key === 'Nomor') returnData.nomor = parseInt(value)
		else if (key === 'Tanggal Disahkan') returnData.tanggalSah = dateStringToISO(value)
		else if (key === 'Tanggal Diundangkan') returnData.tanggalUndang = dateStringToISO(value)
		else if (key === 'LN') returnData.noLN = value.replace('-', '')
		else if (key === 'TLN') returnData.noTLN = value.replace('-', '')
	})

	returnData.abstrak = $('div.link-list:nth-child(4) > div:nth-child(1) > ul:nth-child(2) > li:nth-child(1) > div:nth-child(1)').text().trim()
	if (returnData.abstrak === '-') returnData.bidang = ""

	returnData.bidang = [...$('div.link-list:nth-child(6) > div:nth-child(1) > ul:nth-child(2)').children()].map(el => $(el).text())
	if (returnData.bidang[0] === 'Data tidak ditemukan.') returnData.bidang = []

	returnData.status = [...$('div.link-list:nth-child(8) > div:nth-child(1) > ul:nth-child(2)').children()].map(el => $(el).text())
	if (returnData.status[0] === 'Data tidak ditemukan.') returnData.status = []

	console.log(returnData)
	return returnData

}

const go = async tahun => {

	const masterData = {}

	const response = await axios.get('https://www.dpr.go.id/jdih/uu/year/' + tahun)
	const $ = cheerio.load(response.data)

	for (const el of [...$('tr.list')]) {
		const pdfUrl = "https://www.dpr.go.id" + $(el).find('a[href$=pdf]').attr('href')
		const nomor = Number.parseInt($(el).find('td:nth-of-type(1)').text().trim())
		const tentang = $(el).find('.text').text().trim()
		const judul = `Undang-Undang Republik Indonesia Nomor ${nomor} Tahun ${tahun}`
		const deskripsi = `${judul} tentang ${tentang}`
	
		masterData[`${tahun}-${nomor}`] = {}
		console.log(`${tahun}-${nomor} get!`)

		const indexUrl = "https://www.dpr.go.id" + $(el).find('a:nth-of-type(1)').attr('href')
		const indexData = await getIndex(indexUrl)
	
		masterData[`${tahun}-${nomor}`] = {
			tahun,
			nomor,
			tentang,
			judul,
			deskripsi,
			pdfUrl,
			indexUrl,
			indexData
		}

		await new Promise(r => setTimeout(r, 500));
	}
	fs.outputJSONSync(`data/uu/${tahun}.json`, masterData)

}

for (let i = 1951; i < 2023; i++) {
	await go(i)
}