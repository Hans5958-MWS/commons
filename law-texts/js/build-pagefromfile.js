import axios from "axios"
import fs from "fs-extra"

const pagefromfileFile = 'cats-uu.txt'

fs.outputFileSync(pagefromfileFile, '')

const go = async tahun => {
	const outputText = `{{-start-}}
'''Category:Law of the Republic of Indonesia of ${tahun}'''
[[Category:Law of the Republic of Indonesia|${tahun}]]
[[Category:${tahun} in Indonesia]]
{{-stop-}}
`
	fs.appendFileSync(pagefromfileFile, outputText)
}

for (let i = 1945; i < 2023; i++) {
	await go(i)
}

/*

=={{int:filedesc}}==
{{Information
| Description    = $DESCRIPTOR$
| Source         = $URL$
| Author         = {{id|Pemerintah Indonesia}} {{en|Government of Indonesia}}
| Permission     = {{PD-IDNoCopyright}}
}}

[[Category:Law of the Republic of Indonesia]]

*/